# Ansible Role Linux GUI Tools

## Description

This Role installs some GUI tools for Linux-based workstations

- Guake Terminal
- Remmina
- Terminator
